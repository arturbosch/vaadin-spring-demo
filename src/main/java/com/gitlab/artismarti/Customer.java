package com.gitlab.artismarti;

import org.springframework.data.annotation.Id;

/**
 * @author artur
 */
public class Customer {

	@Id
	private String id;
	private String firstname;
	private String lastname;

	public Customer() {
	}

	public Customer(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public String getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	@Override
	public String toString() {
		return "Customer{" +
				"id='" + id + '\'' +
				", firstname='" + firstname + '\'' +
				", lastname='" + lastname + '\'' +
				'}';
	}
}
