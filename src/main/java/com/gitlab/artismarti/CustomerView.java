package com.gitlab.artismarti;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author artur
 */
@SpringView(name = "customers")
public class CustomerView extends VerticalLayout implements View {

	@Autowired
	private CustomerService customerService;
	private Grid grid;

	@PostConstruct
	protected void init() {
		setSizeFull();

		grid = new Grid();
		grid.setSizeFull();

		grid.addColumn("firstname", String.class);
		grid.addColumn("lastname", String.class);

		addComponent(grid);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		grid.setContainerDataSource(new BeanItemContainer<>(Customer.class, customerService.getCustomers()));
	}

}
