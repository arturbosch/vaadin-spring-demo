package com.gitlab.artismarti;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author artur
 */
@Repository
@RepositoryRestResource(path = "customers")
interface CustomerRepository extends MongoRepository<Customer, String> {

	Optional<Customer> findByFirstname(@Param("name") String name);

	Optional<Customer> findByLastname(@Param("name") String name);

}
