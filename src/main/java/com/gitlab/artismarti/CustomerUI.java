package com.gitlab.artismarti;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author artur
 */
@SpringUI
@Theme("valo")
public class CustomerUI extends UI {

	private Navigator navigator;

	@Autowired
	private SpringViewProvider viewProvider;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		VerticalLayout layout = new VerticalLayout();
		MenuBar menu = new MenuBar();
		Panel viewContent = new Panel();

		layout.addComponents(menu, viewContent);
		layout.setSizeFull();
		viewContent.setSizeFull();
		layout.setExpandRatio(viewContent, 1);

		navigator = new Navigator(this, viewContent);
		navigator.addProvider(viewProvider);

		menu.addItem("Home", event -> navigateTo("home"));
		menu.addItem("Customers", event -> navigateTo("customers"));

		navigateTo("home");

		setContent(layout);
	}

	private void navigateTo(String page) {
		navigator.navigateTo(page);
	}

}
