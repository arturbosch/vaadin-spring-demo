package com.gitlab.artismarti;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

/**
 * @author artur
 */
@SpringView(name = "home")
public class HomeView extends VerticalLayout implements View {

	@PostConstruct
	protected void init() {
		addComponent(new Label("This is the home view!"));
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
	}

}
