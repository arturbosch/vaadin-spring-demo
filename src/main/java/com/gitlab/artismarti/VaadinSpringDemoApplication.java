package com.gitlab.artismarti;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VaadinSpringDemoApplication {

	@Autowired
	private CustomerRepository customerRepository;

	public static void main(String[] args) {
		SpringApplication.run(VaadinSpringDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
			customerRepository.deleteAll();
			customerRepository.save(new Customer("Artur", "Bosch"));
			customerRepository.save(new Customer("Chris", "Ghitulescu"));
			customerRepository.findAll().forEach(System.out::println);
		};
	}
}
